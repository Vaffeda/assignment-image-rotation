#include "bmp.h"
#include "image.h"
#include <malloc.h>

int main(int argc, char *argv[]) {

    if (argc != 3 ) {
        //printf("incorrect argument number\n");
        fputs( "incorrect argument number\n", stderr );
        return 1;
    }
    struct image src, dst;

    FILE *in = fopen(argv[1], "rb");
    from_bmp( in, &src );
    fclose( in );

    dst = rotate( src );

    FILE *out = fopen(argv[2], "wb");
    to_bmp( out, &dst );
    fclose( out );

    if( src.data ) free( src.data );
    if( dst.data ) free( dst.data );

    return 0;
}
