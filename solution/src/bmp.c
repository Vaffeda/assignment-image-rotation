#include "bmp.h"
#include <malloc.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct bmp_header const bmp24_header = {
    .bfType = 19778,
    .bfileSize = 0,
    .bfReserved = 0,
    .bOffBits = sizeof( struct bmp_header ),
    .biSize = 40,
    .biWidth = 0,
    .biHeight = 0,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = 0,
    .biSizeImage = 0,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0
};

void close_file(FILE **file) {
    fclose(*file);
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header hdr;

//    if( !fread( &(hdr), sizeof( struct bmp_header ), 1, in ) ) {
    if( fread( &(hdr), sizeof( struct bmp_header ), 1, in ) != 1 ) {
        close_file( &in );
        return READ_INVALID_HEADER;
    }
    img->height = hdr.biHeight;
    img->width = hdr.biWidth;
    img->data = malloc( sizeof(struct pixel) * hdr.biHeight * hdr.biWidth );
    fseek( in, hdr.bOffBits, SEEK_SET );
    for (int32_t height = img->height - 1; height >= 0 ; height-- ) {
        size_t byte_count = 0;
        for (int32_t width = 0; width < img->width; width++ ) {
            size_t index = height * img->width + width;
//            if( !fread( &(img->data[index]), sizeof( struct pixel ), 1, in ) ){
            if( fread( &(img->data[index]), sizeof( struct pixel ), 1, in ) != 1 ){
                close_file( &in );
                return READ_INVALID_HEADER;
            }
            byte_count = byte_count + sizeof( struct pixel );
        }
        if( byte_count % 4 && height != 0 ) {
            uint32_t offset = 4 - byte_count % 4;
            fseek( in, offset, SEEK_CUR );
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {

    struct bmp_header hdr = bmp24_header;
    hdr.biWidth = (uint32_t)img->width;
    hdr.biHeight = (uint32_t)img->height;
    hdr.biSizeImage = 24 / 8 * hdr.biWidth;
    hdr.biSizeImage = ( hdr.biSizeImage + hdr.biSizeImage % 4 ) * hdr.biHeight;
    hdr.bfileSize = hdr.bOffBits + hdr.biSizeImage;

    // запись заголовка
//    if( !fwrite( &(hdr), sizeof( struct bmp_header ), 1, out )) {
    if( fwrite( &(hdr), sizeof( struct bmp_header ), 1, out )  != 1 ) {
        return WRITE_ERROR;
    }

    // запись битовой матрицы
    for (int32_t height = hdr.biHeight - 1; height >= 0 ; height-- ) {
        size_t byte_count = 0;
        for (int32_t width = 0; width < hdr.biWidth; width++ ) {
            size_t index = height * img->width + width;
//            if( !fwrite( &(img->data[index]), sizeof( struct pixel ), 1, out )) {
            if( fwrite( &(img->data[index]), sizeof( struct pixel ), 1, out ) != 1 ) {
                return WRITE_ERROR;
            }
            byte_count = byte_count + sizeof( struct pixel );
        }
        // проверка кратности четырем и добавление пустых байт
        while( byte_count % 4 ) {
            int8_t blank = 0;
            if( !fwrite(&blank, sizeof(uint8_t), 1, out) ) {
                return WRITE_ERROR;
            }
            byte_count++;
        }
    }
    return WRITE_OK;
}
 
