#include "image.h"
#include <malloc.h>

struct image rotate( struct image const source ){
    struct image dst;
    dst.width = source.height;
    dst.height = source.width;
    dst.data = malloc( sizeof(struct pixel) * source.height * source.width );

    for (int32_t height = 0; height < source.height; height++ ) {
        for (int32_t width = 0; width < source.width; width++) {
            size_t src_index = height * source.width + width;
            size_t dst_index = ( source.width - 1 - width ) * source.height + height;
            dst.data[dst_index] = source.data[src_index];
        }
    }
    return dst;
}
