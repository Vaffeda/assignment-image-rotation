#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdio.h>

struct pixel {
    uint8_t rgbBlue;
    uint8_t rgbGreen;
    uint8_t rgbRed;
};

struct image {
//    struct bmp_header header;
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate( struct image const source );

#endif
